﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    public class Musteri : IKullanici
    {

        public string Soyad { get; set; }
        public string Adres { get; set; }
        public string KullaniciAdi { get; set; }
        public string Ad { get; set; }
    }
}
