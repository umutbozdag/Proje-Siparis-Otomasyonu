﻿namespace Project
{
    partial class Giris
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelKayitOl = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.txtKayitOlKA = new System.Windows.Forms.TextBox();
            this.btnKayitGeriGit = new System.Windows.Forms.Button();
            this.btnKayıtOnay = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtKayıtOlSoyisim = new System.Windows.Forms.TextBox();
            this.txtKayitOlisim = new System.Windows.Forms.TextBox();
            this.txtKayitOlSifreTekrar = new System.Windows.Forms.TextBox();
            this.txtKayitOlSifre = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panelGiris = new System.Windows.Forms.Panel();
            this.btnKayitOl = new System.Windows.Forms.Button();
            this.musteri_rdnBtn = new System.Windows.Forms.RadioButton();
            this.yonetici_giris_rdnBtn = new System.Windows.Forms.RadioButton();
            this.giris_btn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.password = new System.Windows.Forms.TextBox();
            this.username = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelKayitOl.SuspendLayout();
            this.panelGiris.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelKayitOl
            // 
            this.panelKayitOl.Controls.Add(this.label9);
            this.panelKayitOl.Controls.Add(this.txtKayitOlKA);
            this.panelKayitOl.Controls.Add(this.btnKayitGeriGit);
            this.panelKayitOl.Controls.Add(this.btnKayıtOnay);
            this.panelKayitOl.Controls.Add(this.label8);
            this.panelKayitOl.Controls.Add(this.label6);
            this.panelKayitOl.Controls.Add(this.label7);
            this.panelKayitOl.Controls.Add(this.txtKayıtOlSoyisim);
            this.panelKayitOl.Controls.Add(this.txtKayitOlisim);
            this.panelKayitOl.Controls.Add(this.txtKayitOlSifreTekrar);
            this.panelKayitOl.Controls.Add(this.txtKayitOlSifre);
            this.panelKayitOl.Controls.Add(this.label4);
            this.panelKayitOl.Controls.Add(this.label5);
            this.panelKayitOl.Location = new System.Drawing.Point(261, 12);
            this.panelKayitOl.Name = "panelKayitOl";
            this.panelKayitOl.Size = new System.Drawing.Size(649, 430);
            this.panelKayitOl.TabIndex = 25;
            this.panelKayitOl.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(30, 75);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(163, 29);
            this.label9.TabIndex = 32;
            this.label9.Text = "Kullanıcı Adı  :";
            // 
            // txtKayitOlKA
            // 
            this.txtKayitOlKA.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtKayitOlKA.Location = new System.Drawing.Point(208, 75);
            this.txtKayitOlKA.Name = "txtKayitOlKA";
            this.txtKayitOlKA.Size = new System.Drawing.Size(209, 33);
            this.txtKayitOlKA.TabIndex = 31;
            // 
            // btnKayitGeriGit
            // 
            this.btnKayitGeriGit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKayitGeriGit.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnKayitGeriGit.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnKayitGeriGit.Location = new System.Drawing.Point(25, 372);
            this.btnKayitGeriGit.Name = "btnKayitGeriGit";
            this.btnKayitGeriGit.Size = new System.Drawing.Size(127, 42);
            this.btnKayitGeriGit.TabIndex = 30;
            this.btnKayitGeriGit.Text = "<- Geri Git";
            this.btnKayitGeriGit.UseVisualStyleBackColor = true;
            this.btnKayitGeriGit.Click += new System.EventHandler(this.btnGeriGit_Click);
            // 
            // btnKayıtOnay
            // 
            this.btnKayıtOnay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKayıtOnay.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnKayıtOnay.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnKayıtOnay.Location = new System.Drawing.Point(481, 372);
            this.btnKayıtOnay.Name = "btnKayıtOnay";
            this.btnKayıtOnay.Size = new System.Drawing.Size(124, 42);
            this.btnKayıtOnay.TabIndex = 29;
            this.btnKayıtOnay.Text = "Onayla";
            this.btnKayıtOnay.UseVisualStyleBackColor = true;
            this.btnKayıtOnay.Click += new System.EventHandler(this.btnKayıtOnay_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label8.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label8.Location = new System.Drawing.Point(17, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(329, 44);
            this.label8.TabIndex = 28;
            this.label8.Text = "Kayıt Olma Formu";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(29, 164);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(164, 29);
            this.label6.TabIndex = 27;
            this.label6.Text = "Soyisim          :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(34, 117);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(159, 29);
            this.label7.TabIndex = 26;
            this.label7.Text = "İsim                :";
            // 
            // txtKayıtOlSoyisim
            // 
            this.txtKayıtOlSoyisim.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtKayıtOlSoyisim.Location = new System.Drawing.Point(208, 164);
            this.txtKayıtOlSoyisim.Name = "txtKayıtOlSoyisim";
            this.txtKayıtOlSoyisim.Size = new System.Drawing.Size(209, 33);
            this.txtKayıtOlSoyisim.TabIndex = 25;
            this.txtKayıtOlSoyisim.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.btnKayıtOlSoyisim_KeyPress);
            // 
            // txtKayitOlisim
            // 
            this.txtKayitOlisim.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtKayitOlisim.Location = new System.Drawing.Point(208, 117);
            this.txtKayitOlisim.Name = "txtKayitOlisim";
            this.txtKayitOlisim.Size = new System.Drawing.Size(209, 33);
            this.txtKayitOlisim.TabIndex = 24;
            this.txtKayitOlisim.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.btnKayitOlisim_KeyPress);
            // 
            // txtKayitOlSifreTekrar
            // 
            this.txtKayitOlSifreTekrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtKayitOlSifreTekrar.Location = new System.Drawing.Point(208, 268);
            this.txtKayitOlSifreTekrar.Name = "txtKayitOlSifreTekrar";
            this.txtKayitOlSifreTekrar.Size = new System.Drawing.Size(209, 33);
            this.txtKayitOlSifreTekrar.TabIndex = 23;
            this.txtKayitOlSifreTekrar.UseSystemPasswordChar = true;
            // 
            // txtKayitOlSifre
            // 
            this.txtKayitOlSifre.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtKayitOlSifre.Location = new System.Drawing.Point(208, 217);
            this.txtKayitOlSifre.Name = "txtKayitOlSifre";
            this.txtKayitOlSifre.Size = new System.Drawing.Size(209, 33);
            this.txtKayitOlSifre.TabIndex = 22;
            this.txtKayitOlSifre.UseSystemPasswordChar = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(25, 268);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(168, 29);
            this.label4.TabIndex = 21;
            this.label4.Text = "Şifre (Tekrar) :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(34, 217);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(159, 29);
            this.label5.TabIndex = 20;
            this.label5.Text = "Şifre               :";
            // 
            // panelGiris
            // 
            this.panelGiris.Controls.Add(this.btnKayitOl);
            this.panelGiris.Controls.Add(this.musteri_rdnBtn);
            this.panelGiris.Controls.Add(this.yonetici_giris_rdnBtn);
            this.panelGiris.Controls.Add(this.giris_btn);
            this.panelGiris.Controls.Add(this.label3);
            this.panelGiris.Controls.Add(this.password);
            this.panelGiris.Controls.Add(this.username);
            this.panelGiris.Controls.Add(this.label2);
            this.panelGiris.Controls.Add(this.label1);
            this.panelGiris.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelGiris.Location = new System.Drawing.Point(261, 12);
            this.panelGiris.Name = "panelGiris";
            this.panelGiris.Size = new System.Drawing.Size(652, 433);
            this.panelGiris.TabIndex = 26;
            // 
            // btnKayitOl
            // 
            this.btnKayitOl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKayitOl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnKayitOl.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnKayitOl.Location = new System.Drawing.Point(72, 220);
            this.btnKayitOl.Name = "btnKayitOl";
            this.btnKayitOl.Size = new System.Drawing.Size(124, 48);
            this.btnKayitOl.TabIndex = 33;
            this.btnKayitOl.Text = "Kayıt Ol";
            this.btnKayitOl.UseVisualStyleBackColor = true;
            this.btnKayitOl.Click += new System.EventHandler(this.btnKayitOl_Click);
            // 
            // musteri_rdnBtn
            // 
            this.musteri_rdnBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.musteri_rdnBtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.musteri_rdnBtn.Location = new System.Drawing.Point(387, 16);
            this.musteri_rdnBtn.Name = "musteri_rdnBtn";
            this.musteri_rdnBtn.Size = new System.Drawing.Size(121, 40);
            this.musteri_rdnBtn.TabIndex = 32;
            this.musteri_rdnBtn.TabStop = true;
            this.musteri_rdnBtn.Text = "Müşteri ";
            this.musteri_rdnBtn.UseVisualStyleBackColor = true;
            // 
            // yonetici_giris_rdnBtn
            // 
            this.yonetici_giris_rdnBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.yonetici_giris_rdnBtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.yonetici_giris_rdnBtn.Location = new System.Drawing.Point(244, 16);
            this.yonetici_giris_rdnBtn.Name = "yonetici_giris_rdnBtn";
            this.yonetici_giris_rdnBtn.Size = new System.Drawing.Size(126, 44);
            this.yonetici_giris_rdnBtn.TabIndex = 31;
            this.yonetici_giris_rdnBtn.TabStop = true;
            this.yonetici_giris_rdnBtn.Text = "Yönetici ";
            this.yonetici_giris_rdnBtn.UseVisualStyleBackColor = true;
            // 
            // giris_btn
            // 
            this.giris_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.giris_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.giris_btn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.giris_btn.Location = new System.Drawing.Point(295, 218);
            this.giris_btn.Name = "giris_btn";
            this.giris_btn.Size = new System.Drawing.Size(137, 50);
            this.giris_btn.TabIndex = 30;
            this.giris_btn.Text = "Giriş";
            this.giris_btn.UseVisualStyleBackColor = true;
            this.giris_btn.Click += new System.EventHandler(this.giris_btn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(20, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(176, 44);
            this.label3.TabIndex = 29;
            this.label3.Text = "Giriş Yap";
            // 
            // password
            // 
            this.password.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.password.Location = new System.Drawing.Point(202, 154);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(230, 33);
            this.password.TabIndex = 28;
            this.password.UseSystemPasswordChar = true;
            // 
            // username
            // 
            this.username.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.username.Location = new System.Drawing.Point(202, 107);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(230, 33);
            this.username.TabIndex = 27;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(37, 158);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(159, 29);
            this.label2.TabIndex = 26;
            this.label2.Text = "Şifre               :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(39, 107);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(157, 29);
            this.label1.TabIndex = 25;
            this.label1.Text = "Kullanıcı Adı :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label10.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label10.Location = new System.Drawing.Point(36, 255);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(144, 25);
            this.label10.TabIndex = 0;
            this.label10.Text = "Firma Sloganı";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(12, 193);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(231, 39);
            this.label11.TabIndex = 1;
            this.label11.Text = "Geldi Geliyor";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Salmon;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(255, 523);
            this.panel1.TabIndex = 34;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ourProject.Properties.Resources.aasd;
            this.pictureBox1.Location = new System.Drawing.Point(41, 15);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(150, 150);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // Giris
            // 
            this.AcceptButton = this.giris_btn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(949, 523);
            this.Controls.Add(this.panelKayitOl);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelGiris);
            this.Name = "Giris";
            this.Text = "Giriş yap";
            this.Load += new System.EventHandler(this.Giris_Load);
            this.panelKayitOl.ResumeLayout(false);
            this.panelKayitOl.PerformLayout();
            this.panelGiris.ResumeLayout(false);
            this.panelGiris.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelKayitOl;
        private System.Windows.Forms.Button btnKayıtOnay;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtKayıtOlSoyisim;
        private System.Windows.Forms.TextBox txtKayitOlisim;
        private System.Windows.Forms.TextBox txtKayitOlSifreTekrar;
        private System.Windows.Forms.TextBox txtKayitOlSifre;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panelGiris;
        private System.Windows.Forms.Button btnKayitOl;
        private System.Windows.Forms.RadioButton musteri_rdnBtn;
        private System.Windows.Forms.RadioButton yonetici_giris_rdnBtn;
        private System.Windows.Forms.Button giris_btn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox password;
        private System.Windows.Forms.TextBox username;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnKayitGeriGit;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtKayitOlKA;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

