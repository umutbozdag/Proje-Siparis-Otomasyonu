﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    public interface IKullanici
    {
        string Ad { get; set; }
        string Soyad { get; set; }
        string KullaniciAdi { get; set; }
    }
}
