﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Project
{
    public abstract class Odeme
    {
        public decimal Fiyat { get; set; }

        public abstract bool Dogrulama();
    }
}
